`timescale 1ns / 1ps
module lab4(clk,rst_n,segs,ssd_ctl);
input clk,rst_n;
output [3:0]ssd_ctl;//scan control for 7-segment display
output  [7:0]segs;//7 segments display
wire clk_d;//divided clock
wire [1:0]ssd_ctl_en;
wire [3:0]cnt_out,ssd_in;//binary counter output

freqdiv2 A0 (.clk(clk),.rst_n(rst_n),.clk_out(clk_d),.clk_ctl(ssd_ctl_en));
BCD_downcounter A1(.clk(clk_d),.rst_n(rst_n),.q(cnt_out));
scan A2(.ssd_ctl_en(ssd_ctl_en),.ssd_in(ssd_in),.ssd_ctl(ssd_ctl),.in0(cnt_out),.in1(4'b1111),.in2(4'b1111),.in3(4'b1111));
display4 A3(.bin(ssd_in),.segs(segs));
endmodule