`timescale 1ns / 1ps

module freqdiv2(clk_out,clk_ctl,clk,rst_n);
output reg clk_out;
output reg [1:0]clk_ctl;
input clk,rst_n;
reg[14:0]cnt_l;
reg[7:0]cnt_h;
reg[25:0]clk_tempt;
always@(*)
clk_tempt={clk_out,cnt_h,clk_ctl,cnt_l}+1'b1;
always@(posedge clk or negedge rst_n)
begin 
    if(~rst_n) {clk_out,cnt_h,clk_ctl,cnt_l}<=26'd0;
    else {clk_out,cnt_h,clk_ctl,cnt_l}<=clk_tempt;
end
endmodule