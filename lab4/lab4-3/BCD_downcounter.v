`timescale 1ns / 1ps

module BCD_downcounter(clk,rst_n,q);
input clk,rst_n;
output reg signed [3:0]q;
reg signed [3:0]q_tempt;
always@(*)
begin
    if(q==4'd0) q_tempt=4'd9;
    else q_tempt=q+4'b1111;
end
//flipflop
always@(posedge clk or negedge rst_n)
begin
   if(~rst_n) q=4'd0;
   else q=q_tempt;
end
endmodule
