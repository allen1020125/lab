`timescale 1ns / 1ps

//downcounter
module downcounter(clk,rst_n,q);
input signed clk,rst_n;
output reg signed [3:0]q; 
reg signed [3:0]q_tempt;
//combinational logic
always@(*)
begin 
    q_tempt=q+4'b1111;
end
//flipflop
always@(posedge clk or negedge rst_n)
begin
    if(!rst_n) q<=4'b0;
    else q<=q_tempt;
end
endmodule