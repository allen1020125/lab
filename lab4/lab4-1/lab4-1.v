`timescale 1ns / 1ps
module lab4(clk,rst_n,q);
input clk,rst_n;
output [3:0]q;
//wire [3:0]q;//LED output
wire clk_d;//divided clock
freqdiv4 A0 (.clk(clk),.rst_n(rst_n),.clk_out(clk_d));
downcounter A1(.clk(clk_d),.rst_n(rst_n),.q(q));
endmodule