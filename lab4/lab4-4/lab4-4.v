`timescale 1ns / 1ps
module lab4(clk,rst_n,segs,ssd_ctl);
input clk,rst_n;
output [3:0]ssd_ctl;//scan control for 7-segment display
output  [7:0]segs;//7 segments display
wire clk_d;//divided clock
wire [1:0]ssd_ctl_en;
wire [3:0]cnt_out0,cnt_out1,ssd_in;//binary counter output
wire c1,c2;

freqdiv4 A0 (.clk(clk),.rst_n(rst_n),.clk_out(clk_d),.clk_ctl(ssd_ctl_en));
BCD_Up_Counter A1(.q(cnt_out0),. carry(c1),. operate(rst_n),. clk(clk_d),. rst_n(rst_n));
BCD_Up_Counter A2(.q(cnt_out1),. carry(c2),. operate(c1),. clk(clk_d),. rst_n(rst_n));
scan A3(.ssd_ctl_en(ssd_ctl_en),.ssd_in(ssd_in),.ssd_ctl(ssd_ctl),.in0(cnt_out1),.in1(cnt_out0),.in2(4'b1111),.in3(4'b1111));
display4 A4(.bin(ssd_in),.segs(segs));
endmodule