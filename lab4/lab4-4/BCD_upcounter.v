`timescale 1ns / 1ps

module BCD_upcounter(clk,rst_n,q,operate,carry);
input clk,rst_n,operate;
output reg signed [3:0]q;
output reg carry;
reg signed [3:0]q_tempt;
always@(*)
begin
    if(q==4'd9 && operate) 
    begin
	q_tempt=4'd0;
	carry=1'd1;
    end
    else if(operate) 
    begin
	q_tempt=q+4'd1;
	carry=1'd0;
    end
    else
    begin
	q_tempt=q;
	carry=1'd0;
    end
end
//flipflop
always@(posedge clk or negedge rst_n)
begin
   if(~rst_n) q=4'd0;
   else q=q_tempt;
end
endmodule
