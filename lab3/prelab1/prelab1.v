`timescale 1ns / 1ps

module binary_up_counter(q,clk,rst_n);
input clk,rst_n;
output reg [3:0]q; 
reg [3:0]q_tempt;
//combinational logic
always@(*)
begin 
    q_tempt=q+4'd1;
end
//flipflop
always@(posedge clk or negedge rst_n)
begin
    if(!rst_n) q<=4'd0;
    else q<=q_tempt;
end
endmodule
