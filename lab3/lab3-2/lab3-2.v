`timescale 1ns / 1ps

module freqdiv1hz(clk,clk_1,rst_n);
input clk,rst_n;//clk=100M HZ
output reg clk_1;//1HZ clock
reg [26:0]count_50M,count_50M_next;
reg clk_1_next;
always@(*)
  if (count_50M == 27'd50000000)
  begin
    count_50M_next = 27'd0;
    clk_1_next = ~clk_1;
  end
  else
  begin
    count_50M_next = count_50M + 1'b1;
    clk_1_next = clk_1;
  end

// Counter flip-flops
always @(posedge clk or negedge rst_n)
  if (~rst_n)
  begin
    count_50M <=27'b0;
    clk_1 <=1'b0;
  end
  else
  begin
    count_50M <= count_50M_next;
    clk_1 <= clk_1_next;
  end
endmodule
