`timescale 1ns / 1ps

module top(q,clk,rst_n);
output[3:0]q;
input clk,rst_n;
wire clk_d;//divided clock
wire[3:0]q;//LED output
//insert frequency divider
freqdiv A0(.clk_out(clk_d),.clk(clk),.rst_n(rst_n));
//insert ringcounter
//ringcounterfor_top A1(.clk(clk_d),.rst_n(rst_n),.q(q));
binup_counter A2(.clk(clk_d),.rst_n(rst_n),.q(q));
endmodule
