`timescale 1ns / 1ps
//define segment codes
`define SS_0 8'b0000_0011
`define SS_2 8'b0010_0101
`define SS_3 8'b0000_1101
`define SS_n 8'b1010_1011
`define SS_T 8'b1000_0111
`define SS_H 8'b1000_1001
`define SS_U 8'b1100_0001
`define SS_E 8'b1000_0110
module nthu(q,clk,rst_n,ssd_ctl);
output[3:0]ssd_ctl;
output[7:0]q;
input clk,rst_n;

reg [7:0] a,b,c,d,e,f,g,h,i,j;//store nthuee2023

wire[1:0]clk_ctl;    
wire clk_d;//divided clock 1HZ
reg [7:0]q;
reg [3:0]ssd_ctl;
//insert frequency divider
freqdiv A0(.clk_out(clk_d),.clk(clk),.rst_n(rst_n),.clk_ctl(clk_ctl));

always@(posedge clk_d or negedge rst)
begin
	if(~rst_n)
        	begin
		a <= `SS_n;
                b <= `SS_T;
                c <= `SS_H;
                d <= `SS_U;
                e <= `SS_E;
                f <= `SS_E;
		g <= `SS_2;
		h <= `SS_0;
		i <= `SS_2;
		j <= `SS_3;
		end
	else begin
		a<=j;
		b<=a;
		c<=b;
		d<=c;
		e<=d;
		f<=e;
		g<=f;
		h<=g;
		i<=h;
		j<=i;
		end
end

always@(*)
begin
	if(clk_ctl==2'b00) ssd_ctl=4'b0111;
	else if(clk_ctl==2'b01) ssd_ctl=4'b1011;
	else if(clk_ctl==2'b10) ssd_ctl=4'b1101;
	else  ssd_ctl=4'b1110;
end

always@(*)
begin
	if(ssd_ctl=4'b0111) q=a;
	else if(ssd_ctl=4'b1011) q=b;
	else if(ssd_ctl=4'b1101) q=c;
	else q=d;
end
endmodule
