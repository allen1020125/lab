`timescale 1ns / 1ps

module ringcounter(q,clk,rst_n);
output reg [7:0]q;
input clk,rst_n;
always@(posedge clk or negedge rst_n)
begin
    if(~rst_n) q<=8'b11011110;
    else
    begin
    q[6]<=q[7];
    q[5]<=q[6];
    q[4]<=q[5];
    q[3]<=q[4];
    q[2]<=q[3];
    q[1]<=q[2];
    q[0]<=q[1];
    q[7]<=q[0];
    end
end
endmodule