`timescale 1ns / 1ps
ounterfor_top(q,clk,rst_n);
output reg [3:0]q;
input clk,rst_n;
always@(posedge clk or negedge rst_n)
begin
    if(~rst_n) q<=4'b0000;
    else
    begin
    q[2]<=q[3];
    q[1]<=q[2];
    q[0]<=q[1];
    q[3]<=q[0];
    end
end
endmodule