`timescale 1ns / 1ps
`define SS_0 8'b00000011
`define SS_1 8'b10011111
`define SS_2 8'b00100101
`define SS_3 8'b00001101
`define SS_4 8'b10011001
`define SS_5 8'b01001001
`define SS_6 8'b01000001
`define SS_7 8'b00011111
`define SS_8 8'b00000001
`define SS_9 8'b00001001
`define SS_A 8'b00010001
`define SS_B 8'b11000001
`define SS_C 8'b01100011
`define SS_D 8'b10000101
`define SS_E 8'b01100001
`define SS_F 8'b01110001

module lab8(clk,rst_n,PS2_DATA,PS2_CLK,segs,ssd_ctl,last_change);
inout PS2_DATA,PS2_CLK; // B17 C17
input clk,rst_n;
output [7:0]segs;
output [3:0]ssd_ctl;
output [8:0]last_change;

wire [6:0] ssd_in;
wire [6:0] key_num;
wire [511:0] key_down;
wire key_valid;
wire [1:0]ssd_ctl_en;
freqdiv8 A0(.clk_out(),.clk_ctl(ssd_ctl_en),.clk(clk),.rst_n(rst_n));
KeyboardDecoder A1(.key_down(key_down),.last_change(last_change),.key_valid(key_valid),.PS2_DATA(PS2_DATA),.PS2_CLK(PS2_CLK),.rst(~rst_n),.clk(clk));//high active reset
keyboard A2(.keynum(key_num),.clk(clk),.rst_n(rst_n),.key_down(key_down),.last_change(last_change),.key_valid(key_valid));
scan A3(.ssd_ctl_en(ssd_ctl_en),.ssd_in(ssd_in),.ssd_ctl(ssd_ctl),.in0(7'd10),.in1(7'd10),.in2(7'd10),.in3(key_num));
display A4(.ascii(ssd_in),.segs(segs));
endmodule
