`timescale 1ns / 1ps

module keyboard(keynum,clk,rst_n,key_down,last_change,key_valid);
input clk,rst_n,key_valid;
input [8:0]last_change;
input [511:0]key_down;
output reg [6:0]keynum;//ascii
reg[6:0]keynum_next;
always@(*)
begin
    case(last_change)
    9'h01C: keynum_next=7'd97;
    9'h01B: keynum_next=7'd115;
    9'h03A: keynum_next=7'd109;
    9'h05A: keynum_next=7'd10;//ENTER
    default: keynum_next=keynum;
    endcase
end
always@(posedge clk or negedge rst_n)
begin 
    if(~rst_n) keynum<=7'd10;
    else   keynum<=keynum_next;
end
endmodule
