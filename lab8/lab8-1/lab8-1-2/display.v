`timescale 1ns / 1ps
//define segment codes
`define SS_0 8'b00000011
`define SS_1 8'b10011111
`define SS_2 8'b00100101
`define SS_3 8'b00001101
`define SS_4 8'b10011001
`define SS_5 8'b01001001
`define SS_6 8'b01000001
`define SS_7 8'b00011111
`define SS_8 8'b00000001
`define SS_9 8'b00001001
`define SS_A 8'b00010001
`define SS_B 8'b11000001
`define SS_C 8'b01100011
`define SS_D 8'b10000101
`define SS_E 8'b01100001
`define SS_F 8'b01110001
module display(ascii,segs);
input [6:0]ascii;
output reg[7:0]segs;
always@(*)
begin
    case(ascii)
    7'd97: segs=8'b01111111;//own design
    7'd115: segs=8'b10111111;
    7'd109: segs=8'b11011111;
    7'd10: segs=8'b11111111;
    default:segs=8'b00000000;
    endcase
end
endmodule
