`timescale 1ns / 1ps

module keyboard(keynum,clk,rst_n,key_down,last_change,key_valid);
input clk,rst_n,key_valid;
input [8:0]last_change;
input [511:0]key_down;
output reg [3:0]keynum;// 0,1,2,3,4...
reg[3:0]keynum_next;
always@(*)
begin
    case(last_change)
    9'b0_0111_0000: keynum_next=4'd0;
    9'b0_0110_1001: keynum_next=4'd1;
    9'b0_0111_0010: keynum_next=4'd2;
    9'b0_0111_1010: keynum_next=4'd3;
    9'b0_0110_1011: keynum_next=4'd4;
    9'b0_0111_0011: keynum_next=4'd5;
    9'b0_0111_0100: keynum_next=4'd6;
    9'b0_0110_1100: keynum_next=4'd7;
    9'b0_0111_0101: keynum_next=4'd8;
    9'b0_0111_1101: keynum_next=4'd9;
    default: keynum_next=keynum;
    endcase
end
always@(posedge clk or negedge rst_n)
begin 
    if(~rst_n) keynum<=4'd15;
    else   keynum<=keynum_next;
end
endmodule
