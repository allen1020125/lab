`timescale 1ns / 1ps

module scan(ssd_ctl_en,,ssd_in,ssd_ctl,in0,in1,in2,in3);
input [3:0] in0,in1,in2,in3;//binary input control for four digits
input [1:0]ssd_ctl_en;//divided clock for scan control
output reg [3:0]ssd_in,ssd_ctl;//binary data,scan control for 7-segment display
always@(*)
begin
    case(ssd_ctl_en)
    2'b00:
    begin 
        ssd_ctl=4'b0111;
        ssd_in=in0;
    end
    2'b01:
    begin 
        ssd_ctl=4'b1011;
        ssd_in=in1;
    end
    2'b10:
    begin 
        ssd_ctl=4'b1101;
        ssd_in=in2;
    end
    2'b11:
    begin 
        ssd_ctl=4'b1110;
        ssd_in=in3;
    end
    default:
    begin
        ssd_ctl=4'b0000;
        ssd_in=in0;
    end
endcase
end
endmodule

