`define SS_0 8'b00000011
`define SS_1 8'b10011111
`define SS_2 8'b00100101
`define SS_3 8'b00001101
`define SS_4 8'b10011001
`define SS_5 8'b01001001
`define SS_6 8'b01000001
`define SS_7 8'b00011111
`define SS_8 8'b00000001
`define SS_9 8'b00001001
`define SS_A 8'b00010001
`define SS_B 8'b11000001
`define SS_C 8'b01100011
`define SS_D 8'b10000101
`define SS_E 8'b01100001
`define SS_F 8'b01110001
module Lab8_2(segs, BCD,ssd_ctl, PS2_DATA, PS2_CLK, clk, rst_n);
    output reg [7:0] segs;
    output [3:0] BCD;
    output [3:0] ssd_ctl;
    inout PS2_DATA;
    inout PS2_CLK;
    input clk;
    input rst_n;
    
    wire [3:0] ssd_in;
    wire [511:0] key_down;
    wire [8:0] last_change;
    wire key_valid;
    wire read_enable_final_check;
    wire Enter_key;
    wire [15:0] BCD_vector;
    wire [1:0]ssd_ctl_en;

    freqdiv8 A0(.clk_out(),.clk_ctl(ssd_ctl_en),.clk(clk),.rst_n(rst_n));

    KeyboardDecoder A1(
	.key_down(key_down),
	.last_change(last_change),
	.key_valid(key_valid),
	.PS2_DATA(PS2_DATA),
	.PS2_CLK(PS2_CLK),
	.rst(~rst_n),
	.clk(clk)
    );

    keyboard A2(
    .BCD(BCD), 
    .read_enable_final_check(read_enable_final_check),
    .read_enable(key_valid && key_down[last_change]), 
    .last_change(last_change)
    );//read_enable must be all 1

    assign Enter_key = (BCD == 4'd10);

    Adder A4(
    .BCD_Sum(BCD_vector[7:0]),
    .A(BCD_vector[15:12]),
    .B(BCD_vector[11:8]),
    .Enter_key(Enter_key),
    .read_enable_final_check(read_enable_final_check),
    .BCD(BCD),
    .clk(clk),
    .rst_n(rst_n)
    );
    scan A5(.ssd_ctl_en(ssd_ctl_en),.ssd_in(ssd_in),.ssd_ctl(ssd_ctl),.in0(BCD_vector[15:12]),.in1(BCD_vector[11:8]),.in2(BCD_vector[7:4]),.in3(BCD_vector[3:0]));
    display A6(.bin(ssd_in),.segs(segs));
endmodule
