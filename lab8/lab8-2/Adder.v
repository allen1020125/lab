module Adder(BCD_Sum, A, B, Enter_key, read_enable_final_check, BCD, clk, rst_n);
    output reg [7:0] BCD_Sum;
    output reg [3:0] A, B;
    input Enter_key;
    input read_enable_final_check;
    input [3:0] BCD;
    input clk, rst_n;    
    reg [7:0] next_BCD_Sum;
    reg [3:0] next_A, next_B;
    reg refresh, next_refresh;

    //Shift or Refresh
    always@(*)
    begin
        case({read_enable_final_check, Enter_key, refresh})
            3'b111, 3'b011: begin next_A = 4'd0; next_B = 4'd0; end
            3'b100: begin next_A = B; next_B = BCD; end
            default: begin next_A = A; next_B = B; end
        endcase
    end
    //Sum
    always@(*)
    begin
        case({( A + B <= 8'd9 ), Enter_key, refresh})
            3'b111, 3'b011: next_BCD_Sum = 8'd0;
            3'b110: next_BCD_Sum = A + B;
            3'b010: next_BCD_Sum = { 4'd1, A + B - 4'd10 };
            default: next_BCD_Sum = BCD_Sum;
        endcase
    end
    //Refresh the value
    always@(*)
    begin
        if(Enter_key) next_refresh = ~refresh;
        else next_refresh = refresh;
    end
    //DFF
    always@(posedge clk or negedge rst_n)
    begin
        if(~rst_n) {refresh, BCD_Sum, A, B} <= 17'd0;
        else {refresh, BCD_Sum, A, B} <= {next_refresh, next_BCD_Sum, next_A, next_B};
    end

endmodule