`timescale 1ns / 1ps

//define segment codes
`define SS_0 8'b00000011
`define SS_1 8'b10011111
`define SS_2 8'b00100101
`define SS_3 8'b00001101
`define SS_4 8'b10011001
`define SS_5 8'b01001001
`define SS_6 8'b01000001
`define SS_7 8'b00011111
`define SS_8 8'b00000001
`define SS_9 8'b00001001
`define SS_A 8'b00010001
`define SS_B 8'b11000001
`define SS_C 8'b01100011
`define SS_D 8'b10000101
`define SS_E 8'b01100001
`define SS_F 8'b01110001
module timer(in,clk,rst_n,segs,ssd_ctl,q);
input clk,rst_n,in;
output [7:0]segs;
output [3:0]ssd_ctl;
output [15:0]q;//LED output
wire [1:0] ssd_ctl_en;
wire clk_d;//divided clock
wire count_enable;//if count is enabled
wire [3:0]ssd_in;
wire [3:0] digit0,digit1;
//freqency divider 
freqdiv5 A0(.clk(clk),.rst_n(rst_n),.clk_out(clk_d),.clk_ctl(ssd_ctl_en));
fsm_lab5 A1(.clk(clk_d),.rst_n(rst_n),.in(in),.count_enable(count_enable));
BCD_downcounter2 A2(.clk(clk_d),.rst_n(rst_n),.enable(count_enable),.digit0(digit0),.digit1(digit1));
//display block
scan5 A3(.ssd_ctl_en(ssd_ctl_en),.ssd_in(ssd_in),.ssd_ctl(ssd_ctl),.in0(4'b1111),.in1(4'b1111),.in2(digit1),.in3(digit0));
led A4(.led(q),.digit0(digit0),.digit1(digit1));
//binary to 7-segment display decoder
display A5(.bin(ssd_in),.segs(segs));
//assign allzero=((digit1==4'd0)&&(digit0==4'd0));// if all 0 then allzero=1
//assign led={16{allzero}};
//always@(*)
//if(digit0==4'd0&&digit1==4'd0) q=16'b111111111111111;
endmodule
