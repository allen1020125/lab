`timescale 1ns / 1ps

//define segment codes
`define SS_0 8'b00000011
`define SS_1 8'b10011111
`define SS_2 8'b00100101
`define SS_3 8'b00001101
`define SS_4 8'b10011001
`define SS_5 8'b01001001
`define SS_6 8'b01000001
`define SS_7 8'b00011111
`define SS_8 8'b00000001
`define SS_9 8'b00001001
`define SS_A 8'b00010001
`define SS_B 8'b11000001
`define SS_C 8'b01100011
`define SS_D 8'b10000101
`define SS_E 8'b01100001
`define SS_F 8'b01110001
module timer2(in,clk,rst_n,segs,ssd_ctl,q);
input clk,rst_n,in;
output [7:0]segs;
output [3:0]ssd_ctl;
output [15:0]q;//LED output
wire [1:0] ssd_ctl_en;
wire clk_d;//divided clock
wire count_enable;//if count is enabled
wire [3:0]ssd_in;
wire [3:0] digit0,digit1,digit2,digit3;
//freqency divider 
freqdiv5 A0(.clk(clk),.rst_n(rst_n),.clk_out(clk_d),.clk_ctl(ssd_ctl_en));
fsm_lab5 A1(.clk(clk_d),.rst_n(rst_n),.in(in),.count_enable(count_enable));
//stopwatch
stopwatch_lab5 A3(.clk(clk_d),.rst_n(rst_n),.count_enable(count_enable),.sec0(digit0),.sec1(digit1),.min0(digit2),.min1(digit3));
//display block
scan5 A4(.ssd_ctl_en(ssd_ctl_en),.ssd_in(ssd_in),.ssd_ctl(ssd_ctl),.in0(digit3),.in1(digit2),.in2(digit1),.in3(digit0));
led2 A5(.led(q),.digit0(digit0),.digit1(digit1),.digit2(digit2),.digit3(digit3));
//binary to 7-segment display decoder
display A6(.bin(ssd_in),.segs(segs));
endmodule
