`timescale 1ns / 1ps

module fsm_lab5(clk,rst_n,in,count_enable);
input clk,rst_n,in;//in=pressed
output reg count_enable;//1 is countable 
reg state,next_state;//state of fsm
//FSM state decision moore machine
parameter s0=1'b0;
parameter s1=1'b1;
always@(*)
begin
    case(state)
    s0: if(in)
    begin
        next_state=s1;
        count_enable=1;
    end
    else 
    begin
        next_state=s0;
        count_enable=0;
    end
    s1: if(in)
    begin
        next_state=s0;
        count_enable=0;
    end
    else
    begin
       next_state=s1;
       count_enable=1;
    end
    default: 
    begin
        next_state=s0;
        count_enable=0;
    end
    endcase
end
//FSM transition 
always@(posedge clk or posedge rst_n)
begin
    if(rst_n) state<=s0;
    else state<=next_state;
end     
endmodule
