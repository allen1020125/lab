`timescale 1ns / 1ps

module  stopwatch_lab5(sec0,sec1,min0,min1,count_enable,clk,rst_n);
input clk,rst_n,count_enable;
output [3:0] sec0,sec1,min0,min1;
wire borrow_sec0,borrow_sec1,borrow_min0;//borrow indicator
wire allzero;//count to zero indicator
downcounter_timer2 s0(.clk(clk),.rst_n(rst_n),.count_enable((~allzero)&&count_enable),.q(sec0),.count_limit(4'd9),.time_borrow(borrow_sec0),.init_value(4'd9));
downcounter_timer2 s1(.clk(clk),.rst_n(rst_n),.count_enable(borrow_sec0),.q(sec1),.count_limit(4'd5),.time_borrow(borrow_sec1),.init_value(4'd5));
downcounter_timer2 m0(.clk(clk),.rst_n(rst_n),.count_enable(borrow_sec0&&borrow_sec1),.q(min0),.count_limit(4'd5),.time_borrow(borrow_min0),.init_value(4'd9));
downcounter_timer2 m1(.clk(clk),.rst_n(rst_n),.count_enable(borrow_sec0&&borrow_sec1&&borrow_min0),.q(min1),.count_limit(4'd5),.time_borrow(),.init_value(4'd5));
endmodule

