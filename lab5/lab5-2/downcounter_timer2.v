`timescale 1ns / 1ps

module downcounter_timer2(clk,rst_n,count_enable,q,count_limit,time_borrow,init_value);
input clk,rst_n,count_enable;
input[3:0] count_limit,init_value;
output reg time_borrow;
output reg [3:0]q;
reg [3:0]q_next;
//counter register
always@(posedge clk or posedge rst_n)
begin
    if(rst_n) q<=init_value;
    else q<=q_next;
end
always@(*)
begin
    if(count_enable&&q==4'd0)
    begin
        q_next=count_limit;
        time_borrow=1'd1;
    end
    else if(count_enable) q_next=q-1'd1;
    else
    begin
        q_next=q;
        time_borrow=1'd0;
    end 
end   
endmodule

