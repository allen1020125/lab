module Lab2_1(w,x,y,z,a,b,c,d);
    output w,x,y,z;
    input a,b,c,d;

    assign w = ( b & ( c | d )) | a;
    assign x = ( ~ a ) & ( b | c ) | ( a ^ d );
    assign y = ( ~ ( b | c ) ) | ( ~ ( b | d ) ) | ( b & c & d );
    assign z = ( ~ a) & c;
endmodule
