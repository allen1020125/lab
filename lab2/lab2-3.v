module Bonus(bull, cow, secret, guess);

    output reg[2:0] bull, cow;
    input[7:0] secret, guess;
    reg detect;
    
    always@(*)
    begin
        detect = (secret[7:4] == secret[3:0]) | (guess[7:4] == guess[3:0]);
        
        if(detect) bull = 3'b000;
        else if(secret == guess) bull = 3'b100;
        else if( (secret[7:4] == guess[7:4]) | (secret[3:0] == guess[3:0]) ) bull = 3'b010;
        else bull = 3'b001;

        if(detect) cow = 3'b000;
        else if( (secret[7:4] == guess[3:0]) & (secret[3:0] == guess[7:4]) ) cow = 3'b100;
        else if( (secret[7:4] == guess[3:0]) | (secret[3:0] == guess[7:4]) ) cow = 3'b010;
        else cow = 3'b001;
    end


endmodule
