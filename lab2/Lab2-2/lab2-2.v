`timescale 1ns / 1ps

module top3(bin,segs,ssd_ctl,led);
input [3:0]bin;
output [7:0]segs;
output [3:0]ssd_ctl,led;
project_3 A0 (.bin(bin),.segs(segs),.d(led));
assign ssd_ctl=4'b0000;
endmodule
