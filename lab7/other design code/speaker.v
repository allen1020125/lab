`timescale 1ns / 1ps



module speaker(clk,rst_n,audio_mclk,audio_sck,audio_sdin,audio_lrck);
input clk,rst_n;
output audio_mclk,audio_sck,audio_sdin,audio_lrck;
wire [15:0]audio_right,audio_left;
buzzer_control A0(.clk(clk),.rst_n(rst_n),.note_div(22'd191571),.audio_left(audio_left),.audio_right(audio_right));//Do->k=191571 Re->k=170608 Mi->k=151515
speaker_control A1(.clk(clk),.rst_n(rst_n),.audio_in_left(audio_left),.audio_in_right(audio_right),.audio_mclk(audio_mclk),.audio_sck(audio_sck),.audio_sdin(audio_sdin),.audio_lrck(audio_lrck));
endmodule
