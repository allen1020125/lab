`timescale 1ns / 1ps

module buzzer_control(clk,rst_n,note_div,audio_left,audio_right);
input clk,rst_n;
input [21:0]note_div;
output [15:0]audio_left,audio_right;
// Declare internal signals
reg [21:0] clk_cnt_next,clk_cnt;
reg b_clk,b_clk_next;//b-clk �չL���W�v
always@(posedge clk or negedge rst_n)
begin
    if(~rst_n)
        begin
            clk_cnt<=22'd0;
            b_clk<=1'b0;
        end
    else 
        begin
            clk_cnt<=clk_cnt_next;
            b_clk<=b_clk_next;
        end
end        
always@(*)
begin
    if(clk_cnt==note_div)
    begin
        b_clk_next=~b_clk;
        clk_cnt_next=22'd0;
    end
    else
    begin
         b_clk_next=b_clk;
         clk_cnt_next=clk_cnt+1'd1;
    end
end     
assign audio_left=(b_clk==1'b0)?16'hB000:16'h5FFF;
assign audio_right=(b_clk==1'b0)?16'hB000:16'h5FFF;
endmodule
