`timescale 1ns / 1ps

module electronic_organ(segs, ssd_ctl, audio_mclk, audio_lrclk, audio_sclk, audio_sdin, clk, rst_n, c, d, e, f, g, a, b, C, D, E, F, G, A, B);
    output [7:0]segs;
    output [3:0]ssd_ctl;
    output audio_mclk;
    output audio_lrclk;
    output audio_sclk;
    output audio_sdin;
    input clk, rst_n;
    input c, d, e, f, g, a, b, C, D, E, F, G, A, B;    
    wire [15:0]audio_in_left, audio_in_right;
    reg [21:0]note_div;
    reg [7:0]segs;
    
    always@(*)
        case({c, d, e, f, g, a, b, C, D, E, F, G, A, B})
            14'b10000000000000: note_div = 22'd191571;
            14'b01000000000000: note_div = 22'd170648;
            14'b00100000000000: note_div = 22'd151515;
            14'b00010000000000: note_div = 22'd143266;
            14'b00001000000000: note_div = 22'd127551;
            14'b00000100000000: note_div = 22'd113636;
            14'b00000010000000: note_div = 22'd101215;
            14'b00000001000000: note_div = 22'd95419;
            14'b00000000100000: note_div = 22'd85034;
            14'b00000000010000: note_div = 22'd75758;
            14'b00000000001000: note_div = 22'd71633;
            14'b00000000000100: note_div = 22'd63775;
            14'b00000000000010: note_div = 22'd56818;
            14'b00000000000001: note_div = 22'd50607;
            default: note_div = 22'd0;
        endcase
    
    buzzer_control BC(.audio_left(audio_in_left),.audio_right(audio_in_right),.clk(clk),.rst_n(rst_n),.note_div(note_div));
    speaker_control SC(.audio_mclk(audio_mclk),.audio_lrclk(audio_lrclk),.audio_sclk(audio_sclk),.audio_sdin(aud.io_sdin),.audio_in_left(audio_in_left),.audio_in_right(audio_in_right),.clk(clk),.rst_n(rst_n));
    
    assign ssd_ctl = 4'b1110;
    always@(*)
        case({c, d, e, f, g, a, b, C, D, E, F, G, A, B, rst_n})
            15'b100000000000001: segs = 8'b01100011;
            15'b010000000000001: segs = 8'b10000101;
            15'b001000000000001: segs = 8'b01100001;
            15'b000100000000001: segs = 8'b01110001;
            15'b000010000000001: segs = 8'b01000011;
            15'b000001000000001: segs = 8'b00010001;
            15'b000000100000001: segs = 8'b11000001;
            15'b000000010000001: segs = 8'b01100011;
            15'b000000001000001: segs = 8'b10000101;
            15'b000000000100001: segs = 8'b01100001;
            15'b000000000010001: segs = 8'b01110001;
            15'b000000000001001: segs = 8'b01000011;
            15'b000000000000101: segs = 8'b00010001;
            15'b000000000000011: segs = 8'b11000001;
            default: segs = {8{1'b1}};
        endcase
endmodule
