`timescale 1ns / 1ps

module speaker_double_tone(audio_mclk, audio_lrclk, audio_sclk, audio_sdin, clk, rst_n, DoMi, ReFa, MiSo, FaLa, SoSi, left, right);
    output audio_mclk;
    output audio_lrclk;
    output audio_sclk;
    output audio_sdin;
    input clk, rst_n;
    input DoMi, ReFa, MiSo, FaLa, SoSi;
    input left, right;
    
    reg [21:0]note_div_left, note_div_right;
    wire [15:0]audio_in_left, audio_in_right;
    
    always@(*)
        case({left, right})
            2'b10:
                case({DoMi, ReFa, MiSo, FaLa, SoSi})
                    5'b10000:
                    begin
                        note_div_left = 22'd191571;
                        note_div_right = {22{1'b0}};
                    end
                    5'b01000:
                    begin
                        note_div_left = 22'd170648;
                        note_div_right = {22{1'b0}};
                    end                    
                    5'b00100:
                    begin
                        note_div_left = 22'd151515;
                        note_div_right = {22{1'b0}};
                    end
                    5'b00010:
                    begin
                        note_div_left = 22'd143266;
                        note_div_right = {22{1'b0}};
                    end
                    5'b00001:
                    begin
                        note_div_left = 22'd127551;
                        note_div_right = {22{1'b0}};
                    end
                    default:
                    begin
                        note_div_left = {22{1'b0}};
                        note_div_right = {22{1'b0}};
                    end
                endcase
            2'b01:
                case({DoMi, ReFa, MiSo, FaLa, SoSi})
                    5'b10000:
                    begin
                        note_div_left = {22{1'b0}};
                        note_div_right = 22'd151515;
                    end
                    5'b01000:
                    begin
                        note_div_left = {22{1'b0}};
                        note_div_right = 22'd143266;
                    end                    
                    5'b00100:
                    begin
                        note_div_left = {22{1'b0}};
                        note_div_right = 22'd127551;
                    end
                    5'b00010:
                    begin
                        note_div_left = {22{1'b0}};
                        note_div_right = 22'd113636;
                    end
                    5'b00001:
                    begin
                        note_div_left = {22{1'b0}};
                        note_div_right = 22'd101215;
                    end
                    default:
                    begin
                        note_div_left = {22{1'b0}};
                        note_div_right = {22{1'b0}};
                    end
                endcase
            2'b11:

                case({DoMi, ReFa, MiSo, FaLa, SoSi})
                    5'b10000:
                    begin
                        note_div_left = 22'd191571;
                        note_div_right = 22'd151515;
                    end
                    5'b01000:
                    begin
                         note_div_left = 22'd170648;
                         note_div_right = 22'd143266;
                    end                    
                    5'b00100:
                    begin
                        note_div_left = 22'd151515;
                        note_div_right = 22'd127551;
                    end
                    5'b00010:
                    begin
                        note_div_left = 22'd143266;
                        note_div_right = 22'd113636;
                    end
                    5'b00001:
                    begin
                        note_div_left = 22'd127551;
                        note_div_right = 22'd101215;
                    end
                    default:
                    begin
                        note_div_left = {22{1'b0}};
                        note_div_right = {22{1'b0}};
                    end
                endcase
            default:
            begin
                note_div_left = {22{1'b0}};
                note_div_right = {22{1'b0}};
            end                                                                            
        endcase             
    
    buzzer_control BC(.audio_left(audio_in_left),.audio_right(audio_in_right),.clk(clk),.rst_n(rst_n),.note_div_left(note_div_left),.note_div_right(note_div_right));
    speaker_control SC(.audio_mclk(audio_mclk),.audio_lrclk(audio_lrclk),.audio_sclk(audio_sclk),.audio_sdin(audio_sdin),.audio_in_left(audio_in_left),.audio_in_right(audio_in_right),.clk(clk),.rst_n(rst_n));        
endmodule
