`timescale 1ns / 1ps

module buzzer_control(audio_left, audio_right, clk, rst_n, note_div_left, note_div_right);
    output [15:0]audio_left;
    output [15:0]audio_right;
    input clk;
    input rst_n;
    input [21:0]note_div_left, note_div_right;

    reg [21:0]clk_cnt_left_next, clk_cnt_left;
    reg [21:0]clk_cnt_right_next, clk_cnt_right;
    reg b_clk_left, b_clk_left_next;
    reg b_clk_right, b_clk_right_next;

    always @(posedge clk or negedge rst_n)
        if (~rst_n)
        begin
            clk_cnt_left <= 22'd0;
            b_clk_left <= 1'b0;
        end
        else
        begin
            clk_cnt_left <= clk_cnt_left_next;
            b_clk_left <= b_clk_left_next;
        end
    always @(*)
        if (clk_cnt_left == note_div_left)
        begin
            clk_cnt_left_next = 22'd0;
            b_clk_left_next = ~b_clk_left;
        end
        else
        begin
            clk_cnt_left_next = clk_cnt_left + 1'b1;
            b_clk_left_next = b_clk_left;
        end
    
    always @(posedge clk or negedge rst_n)
            if (~rst_n)
            begin
                clk_cnt_right <= 22'd0;
                b_clk_right <= 1'b0;
            end
            else
            begin
                clk_cnt_right <= clk_cnt_right_next;
                b_clk_right <= b_clk_right_next;
            end
    always @(*)
        if (clk_cnt_right == note_div_right)
        begin
            clk_cnt_right_next = 22'd0;
            b_clk_right_next = ~b_clk_right;
        end
        else
        begin
            clk_cnt_right_next = clk_cnt_right + 1'b1;
            b_clk_right_next = b_clk_right;
        end    
      
    assign audio_left = (b_clk_left == 1'b0) ? ((-16'h0AFF) + 1'b1) : 16'h0AFF;
    assign audio_right = (b_clk_right == 1'b0) ? ((-16'h0AFF) + 1'b1) : 16'h0AFF;      
endmodule
