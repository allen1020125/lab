`timescale 1ns / 1ps

module DOREMI(note_div,DO,RE,MI,nosound);
input DO,RE,MI;
output reg [21:0]note_div;
output reg nosound;
always@(*)
begin
     nosound=1'b1;
     note_div=22'd191571;
     if(DO) 
     begin
        nosound=1'b0;
        note_div=22'd191571;
     end
     else if(RE)
     begin
        nosound=1'b0;
        note_div=22'd170608;
     end
     else if(MI) 
     begin
        nosound=1'b0;
        note_div=22'd151515;
     end
end    
endmodule
