`timescale 1ns / 1ps

`define SS_0 8'b00000011
`define SS_1 8'b10011111
`define SS_2 8'b00100101
`define SS_3 8'b00001101
`define SS_4 8'b10011001
`define SS_5 8'b01001001
`define SS_6 8'b01000001
`define SS_7 8'b00011111
`define SS_8 8'b00000001
`define SS_9 8'b00001001
`define SS_A 8'b00010001
`define SS_B 8'b11000001
`define SS_C 8'b01100011
`define SS_D 8'b10000101
`define SS_E 8'b01100001
`define SS_F 8'b01110001
module speaker(clk,rst_n,audio_mclk,audio_sck,audio_sdin,audio_lrck,segs,ssd_ctl,DO,RE,MI,up,down);
input clk,rst_n,DO,RE,MI,up,down;
output audio_mclk,audio_sck,audio_sdin,audio_lrck;
output [7:0]segs;
output [3:0]ssd_ctl;
wire [15:0]audio_right,audio_left;
wire[1:0]ssd_ctl_en;
wire clk_d;//1HZ
wire [15:0]volume;
wire [3:0]volume_cnt0,volume_cnt1;
wire [3:0]ssd_in;
wire up,dowm;
wire nosound;
wire [21:0]note_div;
freqdiv A0 (.clk_out(clk_d),.clk_ctl(ssd_ctl_en),.clk(clk),.rst_n(rst_n));
volume_control A1 (.volume(volume),.volume_cnt0(volume_cnt0),.volume_cnt1(volume_cnt1),.clk(clk_d),.rst_n(rst_n),.up(up),.down(down));
scan A2 (.ssd_ctl_en(ssd_ctl_en),.ssd_in(ssd_in),.ssd_ctl(ssd_ctl),.in0(volume_cnt1),.in1(volume_cnt1),.in2(4'd15),.in3(4'd15));
display A3 (.bin(ssd_in),.segs(segs));
DOREMI A4 (.note_div(note_div),.DO(DO),.RE(RE),.MI(MI),.nosound(nosound));
buzzer_control A5 (.nosound(nosound),.clk(clk),.rst_n(rst_n),.note_div(note_div),.audio_left(audio_left),.audio_right(audio_right),.volume_left(volume),.volume_right(volume));//Do->k=191571 Re->k=170608 Mi->k=151515
speaker_control A6 (.clk(clk),.rst_n(rst_n),.audio_in_left(audio_left),.audio_in_right(audio_right),.audio_mclk(audio_mclk),.audio_sck(audio_sck),.audio_sdin(audio_sdin),.audio_lrck(audio_lrck));
endmodule
