`timescale 1ns / 1ps

module volume_control(volume,volume_cnt0,volume_cnt1,clk,rst_n,up,down);
output reg[15:0] volume;
output reg[3:0] volume_cnt0,volume_cnt1;
input clk,rst_n,up,down;
wire [7:0] volume_cnt; // {volume_cnt1, volume_cnt0}
reg [3:0] volume_cnt0_next;
reg [3:0] volume_cnt1_next;
reg borrow,carry;
wire up_bound,down_bound;
// up to 16, or down to 0 ->16level
assign up_bound = ((volume_cnt0 ==4'd6) && (volume_cnt1 ==4'd1)) ?1'b1:1'b0;
assign down_bound = ((volume_cnt0 ==4'd0) && (volume_cnt1 ==4'd0)) ?1'b1:1'b0;
//LSB counter 
always@(*)
begin
    volume_cnt0_next=volume_cnt0;
    borrow=1'd0;
    carry=1'd0;    
    if(up&&(up_bound==1'b0)&&(volume_cnt0==4'd9)) 
    begin 
        volume_cnt0_next=4'd0;
        carry=1'd1;
    end
    else if(up&&(up_bound==1'b0))
    begin
        volume_cnt0_next=volume_cnt0+4'd1;
        carry=1'd0;
    end
    else if(down&&(down_bound==1'b0)&&(volume_cnt0==4'd0)) 
    begin
        volume_cnt0_next=4'd9;
        borrow=1'd1;
    end
    else if(down&&(down_bound==1'b0))
    begin
         volume_cnt0_next=volume_cnt0-4'd1;
         borrow=1'd0;
    end 
end
//MSB counter
always@(*)
begin
    volume_cnt1_next=volume_cnt1;
    if(carry&&(up_bound==1'b0)) volume_cnt1_next=volume_cnt1+4'd1;
    else if(borrow&&(down_bound==1'b0)) volume_cnt1_next=volume_cnt1-4'd1;    
end
//counter register
always@(posedge clk or negedge rst_n)
begin
    if(~rst_n) 
    begin
        volume_cnt0<=4'b0;
        volume_cnt1<=4'b0;
    end
    else 
    begin
        volume_cnt0<=volume_cnt0_next;
        volume_cnt1<=volume_cnt1_next;
    end
end
assign volume_cnt={volume_cnt1, volume_cnt0};
always@(*)
begin
    case(volume_cnt)
    8'd00: volume=16'h0000;
    8'd01: volume=16'h0800;
    8'd02: volume=16'h1000;
    8'd03: volume=16'h1800;
    8'd04: volume=16'h2000;
    8'd05: volume=16'h2800;
    8'd06: volume=16'h3000;
    8'd07: volume=16'h3800;
    8'd08: volume=16'h4000;
    8'd09: volume=16'h4800;
    8'd10: volume=16'h5000;
    8'd11: volume=16'h5800;
    8'd12: volume=16'h6000;
    8'd13: volume=16'h6800;
    8'd14: volume=16'h7000;
    8'd15: volume=16'h7800;
    8'd16: volume=16'h7FFF;
    default: volume=16'h0000;
    endcase
end    
endmodule
