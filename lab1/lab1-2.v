`timescale 1ns / 1ps

module project_2(a,b,out,v,m);
input signed [3:0]a,b;
output reg signed[3:0]out;
input m;
output reg v;
always@(*)
begin
if(m==0) {v,out}=a+b;
if(m===1)
begin
   {v,out}=a+~b+1;
end
end
endmodule
