`timescale 1ns / 1ps

//upcounter
module timedisplay(sec0,sec1,min0,min1,count_enable,load_value_enable,load_value_sec0,load_value_sec1,load_value_min0,load_value_min1,clk,rst_n);
input clk,rst_n,load_value_enable,count_enable;
output reg [3:0]sec0,sec1,min0,min1;
input [3:0]load_value_sec0,load_value_sec1,load_value_min0,load_value_min1;
wire carry_sec0,carry_sec1,carry_min0;
counter s0 (.clk(clk),.rst_n(rst_n),.q(sec0),.time_carry(carry_sec0),.count_enable(count_enable),.load_value_enable(load_value_enable),.load_value(load_value_sec0),.count_limit(4'd9));
counter s1 (.clk(clk),.rst_n(rst_n),.q(sec1),.time_carry(carry_sec1),.count_enable(carry_sec0),.load_value_enable(load_value_enable),.load_value(load_value_sec1),.count_limit(4'd5));
counter m0 (.clk(clk),.rst_n(rst_n),.q(min0),.time_carry(carry_min0),.count_enable(carry_sec1&&carry_sec0),.load_value_enable(load_value_enable),.load_value(load_value_min0),.count_limit(4'd9));
counter m1 (.clk(clk),.rst_n(rst_n),.q(min1),.time_carry(),.count_enable(carry_min0&&carry_sec1&&carry_sec0),.load_value_enable(load_value_enable),.load_value(load_value_min1),.count_limit(4'd5));
endmodule

