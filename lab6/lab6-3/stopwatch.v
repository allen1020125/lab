`timescale 1ns / 1ps

//downcounter
module stopwatch(led,sec0,sec1,min0,min1,count_enable,load_value_enable,load_value_sec0,load_value_sec1,load_value_min0,load_value_min1,clk,rst_n);
input clk,rst_n,count_enable,load_value_enable;
input [3:0]load_value_sec0,load_value_sec1,load_value_min0,load_value_min1;
output [3:0] sec0,sec1,min0,min1;
output [9:0]led;//LED to indicate counter reaches 0
wire borrow_sec0,borrow_sec1,borrow_min0;//borrow indicator
wire allzero;//count to zero indicator
//all zero detection
assign allzero=((sec0==4'd0)&&(sec1==4'd0)&&(min0==4'd0)&&(min1==4'd0));// if all 0 then allzero=1
assign led={10{allzero}};
downcounter6 s0(.clk(clk),.rst_n(rst_n),.count_enable((~allzero)&&count_enable),.load_value_enable(load_value_enable),.q(sec0),.load_value(load_value_sec0),.count_limit(4'd9),.time_borrow(borrow_sec0));
downcounter6 s1(.clk(clk),.rst_n(rst_n),.count_enable(borrow_sec0),.load_value_enable(load_value_enable),.q(sec1),.load_value(load_value_sec1),.count_limit(4'd5),.time_borrow(borrow_sec1));
downcounter6 m0(.clk(clk),.rst_n(rst_n),.count_enable(borrow_sec0&&borrow_sec1),.load_value_enable(load_value_enable),.q(min0),.load_value(load_value_min0),.count_limit(4'd9),.time_borrow(borrow_min0));
downcounter6 m1(.clk(clk),.rst_n(rst_n),.count_enable(borrow_sec0&&borrow_sec1&&borrow_min0),.load_value_enable(load_value_enable),.q(min1),.load_value(load_value_min1),.count_limit(4'd5),.time_borrow());
endmodule
