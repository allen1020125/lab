`timescale 1ns / 1ps

// FSM State
`define TIME_DISP 5'b00_000
`define TIME_SETMIN 5'b00_001
`define TIME_SETSEC 5'b00_010
`define STW_DISP 5'b01_100
`define STW_SETMIN 5'b01_000
`define STW_SETSEC 5'b01_001
`define STW_START 5'b01_010
`define STW_PAUSE 5'b01_011
`define ALM_DISP 5'b10_000
`define ALM_SETMIN 5'b10_001
`define ALM_SETSEC 5'b10_010
module lab6(led,segs,ssd_ctl,mode,switch,clk,rst_n);
input clk,rst_n,mode,switch;
output[15:0]led;
output[7:0]segs;
output[3:0]ssd_ctl;
wire clk_1,clk_2k;//divided clock generate 1HZ clock,2kHZ clock
wire data_load_enable, reg_load_enable;// ] m   ^  ,// n ] m
wire alarm_enable,stopwatch_count_enable,set_enable;
wire [1:0]set_min_sec;
reg [3:0]sec0,sec1,min0,min1;
wire [3:0]ssd_in;
wire [3:0] time_sec0, time_sec1,time_min0, time_min1; 
wire [3:0] stopwatch_sec0, stopwatch_sec1, stopwatch_min0, stopwatch_min1; 
wire [3:0] alarm_sec0, alarm_sec1,alarm_min0, alarm_min1; 
wire [3:0] reg_q0, reg_q1, reg_q2, reg_q3; 
reg [3:0] reg_load_q0, reg_load_q1, reg_load_q2, reg_load_q3;
wire [4:0]state;
wire [4:0]state_led;
wire [9:0]stopwatch_led,alarm_led;
assign led={state_led,{stopwatch_led|alarm_led},alarm_enable};
//clock generator 
clock_divider C1(.clk_1(clk_1),.clk_100(),.clk_2k(clk_2k),.clk(clk),.rst_n(rst_n));
//time display
timedisplay t1(.sec0(time_sec0),.sec1(time_sec1),.min0(time_min0),.min1(time_min1),.count_enable(1'b1),.load_value_enable(data_load_enable&&(state[4:3]==2'b00)),.load_value_sec0(reg_q0),.load_value_sec1(reg_q1),.load_value_min0(reg_q2),.load_value_min1(reg_q3),.clk(clk_1),.rst_n(rst_n));
//stopwatch
stopwatch s1(.led(stopwatch_led),.sec0(stopwatch_sec0),.sec1(stopwatch_sec1),.min0(stopwatch_min0),.min1(stopwatch_min1),.count_enable(stopwatch_count_enable),.load_value_enable(data_load_enable&&(state[4:3]==2'b01)),.load_value_sec0(reg_q0),.load_value_sec1(reg_q1),.load_value_min0(reg_q2),.load_value_min1(reg_q3),.clk(clk_1),.rst_n(rst_n));
//alarm
alarm a1(.led(alarm_led),.alarm_sec0(alarm_sec0),.alarm_sec1(alarm_sec1),.alarm_min0(alarm_min0),.alarm_min1(alarm_min1),.time_sec0(time_sec0),.time_sec1(time_sec1),.time_min0(time_min0),.time_min1(time_min1),.load_value_enable(data_load_enable&&(state[4:3]==2'b10)),.load_value_sec0(reg_q0),.load_value_sec1(reg_q1),.load_value_min0(reg_q2),.load_value_min1(reg_q3),.clk(clk_1),.rst_n(rst_n),.alarm_enable(alarm_enable));
//FSM
FSM F1(.state_led(state_led),.set_enable(set_enable),.stopwatch_count_enable(stopwatch_count_enable),.data_load_enable(data_load_enable),.reg_load_enable(reg_load_enable),.alarm_enable(alarm_enable),.set_min_sec(set_min_sec),.state(state),.mode(mode),.switch(switch),.clk(clk_1),.rst_n(rst_n));
// Selection which data to load to setting register
always@(*)
begin
    case(state[4:3])
    2'b00:
    begin
        reg_load_q0 = time_sec0;
        reg_load_q1 = time_sec1;
        reg_load_q2 = time_min0;
        reg_load_q3 = time_min1;
    end
    2'b01:
    begin
        reg_load_q0 = stopwatch_sec0;
        reg_load_q1 = stopwatch_sec1;
        reg_load_q2 = stopwatch_min0;
        reg_load_q3 = stopwatch_min1;
    end
    2'b10:
    begin
        reg_load_q0 = alarm_sec0;
        reg_load_q1 = alarm_sec1;
        reg_load_q2 = alarm_min0;
        reg_load_q3 = alarm_min1;
    end
    default:
    begin
        reg_load_q0 = 4'd0;
        reg_load_q1 = 4'd0;
        reg_load_q2 = 4'd0;
        reg_load_q3 = 4'd0;  
    end
endcase
end
//setting register
uniset u1(.q0(reg_q0),.q1(reg_q1),.q2(reg_q2),.q3(reg_q3),.count_enable({set_enable & set_min_sec[1],set_enable & set_min_sec[0]}),.load_value_enable(reg_load_enable),.load_value_q0(reg_load_q0),.load_value_q1(reg_load_q1),.load_value_q2(reg_load_q2),.load_value_q3(reg_load_q3),.clk(clk_1),.rst_n(rst_n));
always@(*)
begin
  case (state)
  `TIME_DISP:
    begin
      sec0 = time_sec0;
      sec1 = time_sec1;
      min0 = time_min0;
      min1 = time_min1;
    end
  `STW_DISP: 
    begin
      sec0 = stopwatch_sec0;
      sec1 = stopwatch_sec1;
      min0 = stopwatch_min0;
      min1 = stopwatch_min1;
    end
  `STW_START:
    begin
      sec0 = stopwatch_sec0;
      sec1 = stopwatch_sec1;
      min0 = stopwatch_min0;
      min1 = stopwatch_min1;
    end
  `STW_PAUSE:
    begin
      sec0 = stopwatch_sec0;
      sec1 = stopwatch_sec1;
      min0 = stopwatch_min0;
      min1 = stopwatch_min1;
    end
  `ALM_DISP:
    begin
      sec0 = alarm_sec0;
      sec1 = alarm_sec1;
      min0 = alarm_min0;
      min1 = alarm_min1;
    end
  `TIME_SETMIN:
    begin
      sec0 = reg_q0;
      sec1 = reg_q1;
      min0 = reg_q2;
      min1 = reg_q3;
    end
  `TIME_SETSEC:
    begin
      sec0 = reg_q0;
      sec1 = reg_q1;
      min0 = reg_q2;
      min1 = reg_q3;
    end
  `STW_SETMIN:
    begin
      sec0 = reg_q0;
      sec1 = reg_q1;
      min0 = reg_q2;
      min1 = reg_q3;
    end
  `STW_SETSEC:
    begin
      sec0 = reg_q0;
      sec1 = reg_q1;
      min0 = reg_q2;
      min1 = reg_q3;
    end
  `ALM_SETMIN:
    begin
      sec0 = reg_q0;
      sec1 = reg_q1;
      min0 = reg_q2;
      min1 = reg_q3;
    end
  `ALM_SETSEC:
    begin
      sec0 = reg_q0;
      sec1 = reg_q1;
      min0 = reg_q2;
      min1 = reg_q3;
    end
  default:
    begin
      sec0 = time_sec0;
      sec1 = time_sec1;
      min0 = time_min0;
      min1 = time_min1;
    end
  endcase
end
//scan control
scan_ctl(.in0(sec0),.in1(sec1),.in2(min0),.in3(min1),.ssd_ctl_en(clk_2k),.rst_n(rst_n),.ssd_in(ssd_in),.ssd_ctl(ssd_ctl));
//  binary to 7-segment display decoder
display6(.bin(ssd_in),.segs(segs));
endmodule


