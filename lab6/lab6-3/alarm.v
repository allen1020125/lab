`timescale 1ns / 1ps

module alarm(led,alarm_sec0,alarm_sec1,alarm_min0,alarm_min1,time_sec0,time_sec1,time_min0,time_min1,load_value_enable,load_value_sec0,load_value_sec1,load_value_min0,load_value_min1,clk,rst_n,alarm_enable);
output reg [9:0]led;
input clk,rst_n,alarm_enable,load_value_enable;
output reg [3:0]alarm_sec0,alarm_sec1,alarm_min0,alarm_min1;
input [3:0]time_sec0,time_sec1,time_min0,time_min1,load_value_sec0,load_value_sec1,load_value_min0,load_value_min1;
reg [3:0] alarm_sec0_next,alarm_sec1_next,alarm_min0_next,alarm_min1_next;
always@(posedge clk or negedge rst_n)
begin
    if(~rst_n) 
    begin
        alarm_sec0<=4'd0;
        alarm_sec1<=4'd0;
        alarm_min0<=4'd0;
        alarm_min1<=4'd0;
    end
    else
    begin
        alarm_sec0<=alarm_sec0_next;
        alarm_sec1<=alarm_sec1_next;
        alarm_min0<=alarm_min0_next;
        alarm_min1<=alarm_min1_next;
    end 
end
always@(*)
begin
    if(load_value_enable) 
    begin
        alarm_sec0_next=load_value_sec0;
        alarm_sec1_next=load_value_sec1;
        alarm_min0_next=load_value_min0;
        alarm_min1_next=load_value_min1;
    end
    else
    begin
         alarm_sec0_next= alarm_sec0;
         alarm_sec1_next= alarm_sec1;
         alarm_min0_next= alarm_min0;
         alarm_min1_next= alarm_min1;
    end
end
always@(*)
begin
    if(alarm_enable&&(alarm_min0==time_min0)&&(alarm_min1==time_min1)) led=10'b1111111111;
    else led=10'b0;
end
endmodule
