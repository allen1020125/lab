`timescale 1ns / 1ps


module counter(clk,rst_n,q,time_carry,count_enable,load_value_enable,load_value,count_limit);
input clk,rst_n,load_value_enable,count_enable;
input [3:0]load_value,count_limit;//value to be loaded,limit of the up counter
output reg [3:0] q;//counter value
output reg time_carry;//counter carry
reg [3:0]q_next;
always@(posedge clk or negedge rst_n)
begin 
    if(~rst_n) q<=4'b0;
    else q<=q_next;
end
always@(*)
begin
    if(load_value_enable) q_next=load_value;
    else if(count_enable&&q==count_limit) 
    begin
        q_next=4'b0;
        time_carry=1'b1;
    end
    else if(count_enable) q_next=q+4'd1;
    else
    begin
        q_next=q;
        time_carry=1'b0;
    end
end
endmodule
