`timescale 1ns / 1ps

module downcounter6(clk,rst_n,count_enable,load_value_enable,q,load_value,count_limit,time_borrow);
input clk,rst_n,count_enable,load_value_enable;
input[3:0]load_value,count_limit;
output reg time_borrow;
output reg [3:0]q;
reg [3:0]q_next;
//counter register
always@(posedge clk or negedge rst_n)
begin
    if(~rst_n) q<=4'd0;
    else q<=q_next;
end
always@(*)
begin
    if(load_value_enable) q_next=load_value;
    else if(count_enable&&q==4'd0)
    begin
        q_next=count_limit;
        time_borrow=1'd1;
    end
    else if(count_enable) q_next=q-1'd1;
    else
    begin
        q_next=q;
        time_borrow=1'd0;
    end 
end   
endmodule
