`timescale 1ns / 1ps

//setting function
module uniset(q0,q1,q2,q3,count_enable,load_value_enable,load_value_q0,load_value_q1,load_value_q2,load_value_q3,clk,rst_n);
input clk,rst_n,load_value_enable;
input [1:0]count_enable;
output [3:0]q0,q1,q2,q3;
input [3:0]load_value_q0,load_value_q1,load_value_q2,load_value_q3;
wire carry_q0,carry_q2;//setting clock
counter uq0(.clk(clk),.rst_n(rst_n),.q(q0),.time_carry(carry_q0),.count_enable(count_enable[0]),.load_value_enable(load_value_enable),.load_value(load_value_q0),.count_limit(4'd9));
counter uq1(.clk(clk),.rst_n(rst_n),.q(q1),.time_carry(),.count_enable(carry_q0),.load_value_enable(load_value_enable),.load_value(load_value_q1),.count_limit(4'd5));
counter uq2(.clk(clk),.rst_n(rst_n),.q(q2),.time_carry(carry_q2),.count_enable(count_enable[1]),.load_value_enable(load_value_enable),.load_value(load_value_q2),.count_limit(4'd9));
counter uq3(.clk(clk),.rst_n(rst_n),.q(q3),.time_carry(),.count_enable(carry_q2),.load_value_enable(load_value_enable),.load_value(load_value_q3),.count_limit(4'd5));
endmodule
