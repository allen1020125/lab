`timescale 1ns / 1ps

// FSM State
`define TIME_DISP 5'b00_000
`define TIME_SETMIN 5'b00_001
`define TIME_SETSEC 5'b00_010
`define STW_DISP 5'b01_100
`define STW_SETMIN 5'b01_000
`define STW_SETSEC 5'b01_001
`define STW_START 5'b01_010
`define STW_PAUSE 5'b01_011
`define ALM_DISP 5'b10_000
`define ALM_SETMIN 5'b10_001
`define ALM_SETSEC 5'b10_010
`define SETMIN 2'b10
`define SETSEC 2'b01
`define TIME 2'b00
`define STW 2'b01
`define ALM 2'b10
module FSM(state_led,set_enable,stopwatch_count_enable,data_load_enable,reg_load_enable,alarm_enable,set_min_sec,state,mode,switch,clk,rst_n);
input clk,mode,rst_n,switch;
output reg [4:0]state_led;
output reg set_enable,stopwatch_count_enable,data_load_enable,reg_load_enable,alarm_enable;
output reg [1:0]set_min_sec;
output reg [4:0]state;
reg [4:0]state_next;
reg [1:0] press_count;
wire [1:0]press_count_next;
reg mode_delay;
wire long_press,short_press;
reg alarm_enable_next;

//  Counter for press time
assign press_count_next = (mode) ? (press_count + 1'd1) : 2'd0;
always @(posedge clk or negedge rst_n)
begin
  if (~rst_n)
    press_count <= 2'd0;
  else
    press_count <= press_count_next;
end
// Mode delay
always @(posedge clk or negedge rst_n)
begin
  if (~rst_n)
    mode_delay <= 1'd0;
  else
    mode_delay <= mode;
end
// Determine short press or long press
assign short_press = (~mode) & mode_delay & (~(press_count[1]&press_count[0]));
assign long_press = (press_count == 2'b11);
//alarm enable register
always@(posedge clk or negedge rst_n)
begin
    if(~rst_n) alarm_enable<=1'b0;
    else  alarm_enable<=alarm_enable_next;
end
//state transition
always@(*)
begin
    set_enable=1'b0;
    stopwatch_count_enable=1'b0;
    data_load_enable=1'b0;
    set_min_sec={2{1'b0}};
    reg_load_enable=1'b0;
    state_next=`TIME_DISP;
    alarm_enable_next = alarm_enable;
    state_led=state;
    case(state)
    `TIME_DISP:
    begin
        state_led=`TIME_DISP;
        if(long_press)
        begin
            state=`TIME_SETMIN;
            reg_load_enable=1'b1;
        end
        else if(short_press)  state=`STW_DISP;
        else state=`TIME_DISP;
    end
      `TIME_SETMIN:
      begin
        state_led = `TIME_SETMIN;
        set_enable = switch;
        set_min_sec = `SETMIN;
        if (short_press)
          state_next = `TIME_SETSEC;
        else if (long_press)
        begin
          state_next =  `TIME_DISP;
          data_load_enable = 1'b1;
        end
        else
          state_next = `TIME_SETMIN;
      end
    `TIME_SETSEC:
      begin
        state_led = `TIME_SETSEC;
        set_enable = switch;
        set_min_sec = `SETSEC;
        if (short_press)
          state_next = `TIME_SETMIN;
        else if (long_press)
        begin
          state_next =  `TIME_DISP;
          data_load_enable = 1'b1;
        end
        else
          state_next = `TIME_SETSEC;
      end
    `STW_DISP:
      begin
        state_led = `STW_DISP;
        if (long_press)
        begin
          state_next = `STW_SETMIN;
          reg_load_enable = 1'b1;
        end
        else if (short_press)
          state_next = `ALM_DISP;
        else if (switch)
        begin
          state_next = `STW_START;
          stopwatch_count_enable = 1'b1;
        end
        else
          state_next = `STW_DISP;
      end
    `STW_SETMIN:
      begin
        state_led = `STW_SETMIN;
        set_enable = switch;
        set_min_sec = `SETMIN;
        if (short_press)
          state_next = `STW_SETSEC;
        else if (long_press)
        begin
          state_next =  `STW_DISP;
          data_load_enable = 1'b1;
        end
        else
          state_next = `STW_SETMIN;
      end
    `STW_SETSEC:
      begin
        state_led = `STW_SETSEC;
        set_enable = switch;
        set_min_sec = `SETSEC;
        if (short_press)
          state_next = `STW_SETMIN;
        else if (long_press)
        begin
          state_next =  `STW_DISP;
          data_load_enable = 1'b1;
        end
        else
          state_next = `STW_SETSEC;
      end
    `STW_START:
      begin
        state_led = `STW_START;
        stopwatch_count_enable = 1'b1;
        if (switch)
          state_next = `STW_PAUSE;
        else
          state_next = `STW_START;
      end
    `STW_PAUSE:
      begin
        state_led = `STW_PAUSE;
        stopwatch_count_enable =1'b1;
        if (switch)
          state_next = `STW_START;
        else if (long_press)
        begin
          state_next = `STW_DISP;
          data_load_enable = 1'b1;
        end
        else
          state_next = `STW_PAUSE;
      end
    `ALM_DISP:
      begin
        state_led = `ALM_DISP;
        if (long_press)
        begin
          state_next = `ALM_SETMIN;
          reg_load_enable = 1'b1;
        end
        else if (short_press)
          state_next = `TIME_DISP;
        else if (switch)
        begin
          state_next = `ALM_DISP;
          alarm_enable_next = ~ alarm_enable;
        end
        else
          state_next = `ALM_DISP;
      end
    `ALM_SETMIN:
      begin
        state_led = `ALM_SETMIN;
        set_enable = switch;
        set_min_sec = `SETMIN;
        if (short_press)
          state_next = `ALM_SETSEC;
        else if (long_press)
        begin
          state_next =  `ALM_DISP;
          data_load_enable = 1'b1;
        end
        else
          state_next = `ALM_SETMIN;
      end
    `ALM_SETSEC:
      begin
        state_led = `ALM_SETSEC;
        set_enable = switch;
        set_min_sec = `SETSEC;
        if (short_press)
          state_next = `ALM_SETMIN;
        else if (long_press)
        begin
          state_next =  `ALM_DISP;
          data_load_enable = 1'b1;
        end
        else
          state_next = `ALM_SETSEC;
      end
    endcase
  end
//state register
always@(posedge clk or negedge rst_n)
begin
    if(~rst_n) state=5'b0;
    else state=state_next;
end
endmodule
