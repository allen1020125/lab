`timescale 1ns / 1ps

`define SS_0 8'b00000011
`define SS_1 8'b10011111
`define SS_2 8'b00100101
`define SS_3 8'b00001101
`define SS_4 8'b10011001
`define SS_5 8'b01001001
`define SS_6 8'b01000001
`define SS_7 8'b00011111
`define SS_8 8'b00000001
`define SS_9 8'b00001001
`define SS_A 8'b00010001
`define SS_B 8'b11000001
`define SS_C 8'b01100011
`define SS_D 8'b10000101
`define SS_E 8'b01100001
`define SS_F 8'b01110001
`define SSD_DEF 8'b0000_0000 // default, all LEDs being lighted

module scan_ctl(in0,in1,in2,in3,ssd_ctl_en,rst_n,ssd_in,ssd_ctl);
input [3:0] in0,in1,in2,in3;
input ssd_ctl_en,rst_n;//divided clock for scan control
output reg [3:0]ssd_ctl,ssd_in;//ssd display control signal ,output to ssd display
reg [1:0] ssd_enable_index;
wire [1:0] ssd_enable_index_next;
//2-bit binary up counter for scan control
assign ssd_enable_index_next=ssd_enable_index+1'd1;
always@(posedge ssd_ctl_en or negedge rst_n)
begin 
    if(~rst_n) ssd_enable_index=2'd0;
    else  ssd_enable_index=ssd_enable_index_next;
end
always@(*)
begin
    case(ssd_enable_index)
    2'b00: 
    begin
        ssd_in=in0;
        ssd_ctl=4'b0111;
    end
    2'b01:
    begin
        ssd_in=in1;
        ssd_ctl=4'b1011;
    end
    2'b10: 
    begin
        ssd_in=in2;
        ssd_ctl=4'b1101;
    end
    2'b11: 
    begin 
        ssd_in=in3;
        ssd_ctl=4'b1110;
    end
    default:
    begin
        ssd_in=in0;
        ssd_ctl=4'b1111;
    end
    endcase
end
endmodule
