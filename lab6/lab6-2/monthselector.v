`timescale 1ns / 1ps

module monthselector1(m1, m0, d1, r);
    input [3:0] m1;
    input [3:0] m0;
    input [3:0] d1;
    output [3:0] r;
    reg [3:0] r;
always@(*)
begin
        case(m1)
            4'd0: begin
                case(m0)
                    4'd1: begin  
                        if(d1 == 4'd3)
                            r = 4'd1;
                        else
                            r = 4'd9;
                    end
                    4'd2: begin  
                        if(d1 == 4'd2)
                            r = 4'd8;
                        else
                            r = 4'd9;
                    end
                    4'd3: begin 
                        if(d1 == 4'd3)
                            r = 4'd1;
                        else
                            r = 4'd9;
                    end
                    4'd4: begin 
                        if(d1 == 4'd3)
                            r = 4'd0;
                        else
                            r = 4'd9;
                    end
                    4'd5: begin  
                        if(d1 == 4'd3)
                            r = 4'd1;
                        else
                            r = 4'd9;    
                    end
                    4'd6: begin  
                        if(d1 == 4'd3)
                            r = 4'd0;
                        else
                            r = 4'd9;
                    end
                    4'd7: begin 
                        if(d1 == 4'd3)
                            r = 4'd1;
                        else
                            r = 4'd9;
                        end
                    4'd8: begin 
                        if(d1 == 4'd3)
                            r = 4'd1;
                        else
                            r = 4'd9;                        
                        end
                    default: begin  
                        if(d1 == 4'd3)
                            r = 4'd0;
                        else
                            r = 4'd9;
                    end
                endcase  
            end
            default: begin//m1=4'd1
                case(m0)
                    4'd0: begin 
                        if(d1 == 4'd3)
                            r = 4'd1;
                        else
                            r = 4'd9;
                    end
                    4'd1: begin  
                        if(d1 == 4'd3)
                            r = 4'd0;
                        else
                            r = 4'd9;
                    end
                    default: begin 
                        if(d1 == 4'd3)
                            r = 4'd1;
                        else
                            r = 4'd9;
                    end
                endcase  
            end            
        endcase 
end
endmodule
