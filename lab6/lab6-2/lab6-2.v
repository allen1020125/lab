`timescale 1ns / 1ps

//define segment codes
`define SS_0 8'b00000011
`define SS_1 8'b10011111
`define SS_2 8'b00100101
`define SS_3 8'b00001101
`define SS_4 8'b10011001
`define SS_5 8'b01001001
`define SS_6 8'b01000001
`define SS_7 8'b00011111
`define SS_8 8'b00000001
`define SS_9 8'b00001001
`define SS_A 8'b00010001
`define SS_B 8'b11000001
`define SS_C 8'b01100011
`define SS_D 8'b10000101
`define SS_E 8'b01100001
`define SS_F 8'b01110001
module eleclock_ymd(clk,rst_n,switch,segs,ssd_ctl);
input clk,rst_n,switch;
output [7:0]segs;
output [3:0]ssd_ctl;
wire [1:0]ssd_ctl_en;
wire [3:0]ssd_in;
wire [3:0]d0,d1,m0,m1,y0,y1;
wire [3:0]r1,r2,r3;
wire c0,c1,c2,c3,c4;
wire clk_d;
freqdiv6 A0(.clk(clk),.rst_n(rst_n),.clk_out(clk_d),.clk_ctl(ssd_ctl_en));
up_counter6 A2(.init_value(4'd0),.value(d0),.carry(c0),.increase(1'b1),.limit(r1),.clk(clk_d),.rst_n(rst_n));
up_counter6 A3(.init_value(4'd0),.value(d1),.carry(c1),.increase(c0),.limit(r2),.clk(clk_d),.rst_n(rst_n));
up_counter6 A4(.init_value(4'd1),.value(m0),.carry(c2),.increase(c1),.limit(r3),.clk(clk_d),.rst_n(rst_n));
up_counter6 A5(.init_value(4'd0),.value(m1),.carry(c3),.increase(c2),.limit(4'd1),.clk(clk_d),.rst_n(rst_n));
up_counter6 A6(.init_value(4'd0),.value(y0),.carry(c4),.increase(c3),.limit(4'd9),.clk(clk_d),.rst_n(rst_n));
up_counter6 A7(.init_value(4'd0),.value(y1),.carry(),.increase(c4),.limit(4'd9),.clk(clk_d),.rst_n(rst_n));
monthselector1(.m1(m1),.m0(m0),.d1(d1),.r(r1));
monthselector2(.m1(m1),.m0(m0),.d1(d1),.r(r2));
monthselector3(.m1(m1),.m0(m0),.d1(d1),.r(r3));
scan6ymd A8(.switch(switch),.ssd_ctl_en(ssd_ctl_en),.ssd_ctl(ssd_ctl),.ssd_in(ssd_in),.y1(y1),.y0(y0),.m1(m1),.m0(m0),.d1(d1),.d0(d0));
display6 A9(.bin(ssd_in),.segs(segs));
endmodule
