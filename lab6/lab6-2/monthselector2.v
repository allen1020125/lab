`timescale 1ns / 1ps

module monthselector2(m1, m0, d1, r);
input [3:0] m1;
input [3:0] m0;
input [3:0] d1;
output [3:0] r;
reg [3:0] r;

always@(*)
begin
    case(m1)
        4'b0:
        begin
            if(m0==4'd2) r=4'd2;
            else r=4'd3;
        end
        default: r=4'd3;
        endcase
end
endmodule
