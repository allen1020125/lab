`timescale 1ns / 1ps

module monthselector3(m1, m0, d1, r);
input [3:0] m1;
input [3:0] m0;
input [3:0] d1;
output [3:0] r;
reg [3:0] r;

always@(*)
begin
    if(m1==4'd0) r=4'd9;
    else r=4'd2;
end
endmodule
