`timescale 1ns / 1ps

//define segment codes
`define SS_0 8'b00000011
`define SS_1 8'b10011111
`define SS_2 8'b00100101
`define SS_3 8'b00001101
`define SS_4 8'b10011001
`define SS_5 8'b01001001
`define SS_6 8'b01000001
`define SS_7 8'b00011111
`define SS_8 8'b00000001
`define SS_9 8'b00001001
`define SS_A 8'b00010001
`define SS_B 8'b11000001
`define SS_C 8'b01100011
`define SS_D 8'b10000101
`define SS_E 8'b01100001
`define SS_F 8'b01110001
module eleclock(clk,rst_n,switch,segs,ssd_ctl);
input clk,rst_n,switch;
output [7:0]segs;
output [3:0]ssd_ctl;
wire [1:0]ssd_ctl_en;
wire [3:0]ssd_in;
wire [3:0]sec0;
wire [3:0]sec1;
wire [3:0]min0;
wire [3:0]min1;
wire [3:0]hr0;
wire [3:0]hr1;
wire c0,c1,c2,c3,c4;
reg [3:0] l;
always@(*)
begin
    if(hr1==4'd2) l=4'd3;
    else l=4'd9;
end
//output [15:0]q;//LED output
freqdiv6 A0(.clk(clk),.rst_n(rst_n),.clk_out(clk_d),.clk_ctl(ssd_ctl_en));
//FSM6 A1(.clk(clk_d),.rst_n(rst_n),.switch(switch),.state(state));
up_counter6 A2(.init_value(4'd0),.value(sec0),.carry(c0),.increase(1'b1),.limit(4'd9),.clk(clk_d),.rst_n(rst_n));
up_counter6 A3(.init_value(4'd0),.value(sec1),.carry(c1),.increase(c0),.limit(4'd5),.clk(clk_d),.rst_n(rst_n));
up_counter6 A4(.init_value(4'd0),.value(min0),.carry(c2),.increase(c1),.limit(4'd9),.clk(clk_d),.rst_n(rst_n));
up_counter6 A5(.init_value(4'd0),.value(min1),.carry(c3),.increase(c2),.limit(4'd5),.clk(clk_d),.rst_n(rst_n));
up_counter6 A6(.init_value(4'd0),.value(hr0),.carry(c4),.increase(c3),.limit(l),.clk(clk_d),.rst_n(rst_n));
up_counter6 A7(.init_value(4'd0),.value(hr1),.carry(),.increase(c4),.limit(4'd2),.clk(clk_d),.rst_n(rst_n));
scan6 A8(.switch(switch),.ssd_ctl_en(ssd_ctl_en),.ssd_ctl(ssd_ctl),.ssd_in(ssd_in),.hr0(hr0),.hr1(hr1),.min0(min0),.min1(min1),.sec0(sec0),.sec1(sec1));
display6 A9(.bin(ssd_in),.segs(segs));
endmodule
