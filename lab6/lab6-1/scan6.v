`timescale 1ns / 1ps

module scan6(switch,ssd_ctl_en,ssd_ctl,hr0,hr1,min0,min1,sec0,sec1,ssd_in);
input [3:0] hr0,hr1,min0,min1,sec0,sec1;//binary input control for four digits
input switch;
input [1:0]ssd_ctl_en;//divided clock for scan control
output reg [3:0]ssd_ctl,ssd_in;//binary data,scan control for 7-segment display
reg [2:0]a;
always@(*) a={switch,ssd_ctl_en};
always@(*)
begin
    case(a)
    3'b100:
    begin 
        ssd_ctl=4'b0111;
        ssd_in=hr1;
    end
    3'b101:
    begin 
        ssd_ctl=4'b1011;
        ssd_in=hr0;
    end
    3'b110:
    begin 
        ssd_ctl=4'b1101;
        ssd_in=min1;
    end
    3'b111:
    begin 
        ssd_ctl=4'b1110;
        ssd_in=min0;
    end
    3'b000:
    begin
        ssd_ctl=4'b1101;
        ssd_in=sec1;
    end
    3'b001:
    begin
        ssd_ctl=4'b1110;
        ssd_in=sec0;
    end
    default:
    begin
        ssd_ctl=4'b1111;
        ssd_in=hr1;
    end 
endcase
end
endmodule
