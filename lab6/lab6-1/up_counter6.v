`timescale 1ns / 1ps

module up_counter6(init_value,value,carry,increase,limit,clk,rst_n);
input clk,rst_n,increase;//limit for counter ,count enable
input [3:0] limit,init_value;//limit for counter ,initial value
output [3:0]value;//counter output ,borrow indicator
output carry;//,borrow indicator
//in always block
reg [3:0] value_tempt,value;
reg carry;
always@(*)
begin 
    if(value==limit && increase)
    begin
        value_tempt=init_value;
        carry=1'b1;
    end
    else if(value!=limit && increase)
    begin
        value_tempt=value+4'b1;
        carry=1'b0;
    end
    else
    begin
        value_tempt=value;
        carry=1'b0;
    end
end
//register part for BCDcounter
always@(posedge clk or posedge rst_n)
begin 
    if(rst_n) value<=init_value;
    else value<=value_tempt;
end       
endmodule
